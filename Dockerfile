FROM openjdk:15.0.1-jdk-slim-buster
ADD target/springboot-docker-diary-cli-0.0.1-SNAPSHOT.jar .
EXPOSE 12121
CMD java -jar springboot-docker-diary-cli-0.0.1-SNAPSHOT.jar
