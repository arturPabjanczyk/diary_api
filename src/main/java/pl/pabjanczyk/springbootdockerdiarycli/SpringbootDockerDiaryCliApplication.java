package pl.pabjanczyk.springbootdockerdiarycli;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootDockerDiaryCliApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootDockerDiaryCliApplication.class, args);
    }

}
