package pl.pabjanczyk.springbootdockerdiarycli;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Entry {

    private Long id;

    private String text;

    public Entry(String text) {
        this.text = text;
    }
}
