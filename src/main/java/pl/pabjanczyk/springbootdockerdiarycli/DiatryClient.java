package pl.pabjanczyk.springbootdockerdiarycli;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class DiatryClient {

    @GetMapping("/test")
    public Entry[] get() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Entry[]> exchange = restTemplate.exchange("http://diary-api:10101/entries",
                HttpMethod.GET,
                HttpEntity.EMPTY,
                Entry[].class);
        return exchange.getBody();
    }
}
